import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { FireService } from '../../services/firestore.service';
import { dataUser } from '../../models/dataUser';
import { UserStateService } from '../../services/state.user.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginData: dataUser = {
    username: 'lardows',
    password: '123456',
    admin: false
  }
  constructor(private fireService: FireService,
              private userStateService: UserStateService,
              private router: Router,
              private alertCtrl: AlertController) { }

  ngOnInit() {}

  async processForm( forma:NgForm ) {
    
    if(forma.valid) {
      this.fireService.login(forma.value).subscribe( async (data) => {
        console.log(data);
        if(data.length) {
          this.userStateService.login(data[0]);
          this.router.navigate(['/main']);
          /*this.router.navigate(['/main']).then( (e) => {
            if (e) {
              console.log("Navigation is successful!");
            } else {
              console.log("Navigation has failed!");
            }
          });*/
        } else {
          const alert = await this.alertCtrl.create({
            header: 'Alerta',
            subHeader: 'Datos incorrectos',
            message: 'El usuario o la contraseña son incorrectos',
            buttons: ['Aceptar']
          });
      
          alert.present();
        }
      });

      /*this.fireStore.collection<any>('usuarios', ref =>
          ref.where('username', '==', 'lardows').where('password', '==', '12345'))
        .valueChanges().subscribe((data) => {
          console.log(data);
      });*/
    } else {
      // Mostrar alerta error
      const alert = await this.alertCtrl.create({
        header: 'Alerta',
        subHeader: 'Requisitos incumplidos',
        message: 'El usuario o la contraseña no cumple requisitos.',
        buttons: ['Aceptar']
      });
  
      alert.present();
    }
  }

}
