import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserStateService } from 'src/app/services/state.user.service';


@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {
  admin:boolean = false;
  constructor(private router: Router,
              private userStateService: UserStateService) { 
              }

  ngOnInit() {
    const data = this.userStateService.getUser().subscribe(data => {
      console.log(data);
      if(!data.data.username.length) {
        this.router.navigate(['/login']);
      } else {
        this.admin = data.data.admin;
      }
    })
  }

}
