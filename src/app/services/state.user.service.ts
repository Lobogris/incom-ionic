import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";

import { Observable } from "rxjs";

import { dataUser } from "../models/dataUser";
import * as UserActions from "../store/actions/userlogin.actions";
import { State, getItem, getUser } from "../store/reducers";

@Injectable({
  providedIn: "root"
})
export class UserStateService {
  public user: Observable<dataUser>;

  constructor(private store: Store<State>) {
    this.user = this.store.select(getItem);
  }

  login(data): void {
    let dataUser:dataUser = {
      username: data.username,
      password: data.password,
      admin: data.admin
    };
    this.store.dispatch(new UserActions.LoginUser({ dataUser }));
  }

  logout(data): void {
    let dataUser:dataUser = {
      username: '',
      password: '',
      admin: false
    };

    this.store.dispatch(new UserActions.LogoutUser({ dataUser }));
  }
  // Dos formas de hacerlo
  getUser(): any {
    return this.store.select(getUser)
  }
  getItem(): any {
    return this.store.select(getItem)
  }
}