import { Action } from "@ngrx/store";
import { dataUser } from "../../models/dataUser";

export enum ActionTypes {
  LoginUser = "Login",
  LogoutUser = "Logout"
}

export class LoginUser implements Action {
  readonly type = ActionTypes.LoginUser;

  constructor(public payload: { dataUser: dataUser }) {}
}

export class LogoutUser implements Action {
  readonly type = ActionTypes.LogoutUser;

  constructor(public payload: { dataUser: dataUser }) {}
}

export type ActionsUnion = LoginUser | LogoutUser;