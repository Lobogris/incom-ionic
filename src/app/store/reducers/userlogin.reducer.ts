import * as fromUser from "../actions/userlogin.actions";
import { dataUser } from "../../models/dataUser";

export interface userState {
  data: dataUser
};

export const initialState: userState = {  
  data: {
    username:'',
    password: '',
    admin: false
  }
};

export function reducer(
  state = initialState,
  action: fromUser.ActionsUnion
): userState {
  switch (action.type) {
    case fromUser.ActionTypes.LoginUser: {
      console.log(state, action.payload)
      return {
        data: {
          username: action.payload.dataUser.username,
          password: action.payload.dataUser.password,
          admin: action.payload.dataUser.admin
        }        
      }
    }
    case fromUser.ActionTypes.LogoutUser: {
      console.log(state, action.payload)
      return {
        data: {
          username:'',
          password:'',
          admin: false
        }        
      };
    }
    default: {
      return state;
    }
  }
}

export const getUser = (state: userState) => state.data;
