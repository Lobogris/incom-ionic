import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../../environments/environment';

import * as fromUser from './userlogin.reducer'

export interface State {
  user: fromUser.userState;
}

export const reducers: ActionReducerMap<State> = {
  user: fromUser.reducer
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];

export const getUser = (state: State) => state.user;

export const getItem = createSelector(
  getUser,
  fromUser.getUser
);

